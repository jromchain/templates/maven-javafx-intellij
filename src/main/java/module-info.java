module com.chainsword.FXTemplate {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;


    opens com.chainsword.FXTemplate to javafx.fxml;
    exports com.chainsword.FXTemplate;
}