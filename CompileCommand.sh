#!/bin/bash
./getJava.sh
export JAVA_HOME=./java
./mvnw clean
./mvnw compile
./mvnw package
TEMP=$(cd target || exit; ls ./*-spring-boot.jar; cd ..);
[[ $TEMP =~ ([^/]+)-spring-boot.jar ]]
rm ./releases/FXTemplate-Latest-Testing.jar
cp ./target/*-spring-boot.jar ./releases/FXTemplate-Latest-Testing.jar
mv ./target/*-spring-boot.jar ./releases/"${BASH_REMATCH[1]}.jar"
./mvnw clean